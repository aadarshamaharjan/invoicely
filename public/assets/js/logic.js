let items = new Array();
const clearInvoice = () => {
    items=[];
    updateTable();
}
const addItems = () => {
    let name = document.getElementById("item-name").value;
    let price = document.getElementById("item-price").value;
    let quantity = document.getElementById("item-quantity").value;
    if (validatePositivity(price, quantity)&&validateEmpty(name,price,quantity)) {
        items.push([name, price, quantity, price * quantity]);
        document.getElementById("item-name").value = "";
        document.getElementById("item-price").value = "";
        document.getElementById("item-quantity").value = "";
        closeDiv();
        updateTable();
    }
}
const updateTable = () => {
    const invoiceTable = document.getElementById('invoice');
    const columns = invoiceTable.rows[0].cells.length;
    const tablebody = document.getElementById('table-body');
    tablebody.innerHTML = "";
    items.map((item, index) => {
        const tr = tablebody.insertRow(index);
        for (i = 0; i < columns; i++) {
            let td = document.createElement('td');
            td = tr.insertCell(i);
            if (i == 0) {
                td.innerHTML = index + 1;
            } else if (i == 4) {
                const actionDiv = document.createElement('div');
                actionDiv.setAttribute('class', 'actionBtn');
                td.appendChild(actionDiv);
                const ele = document.createElement('input');
                ele.setAttribute('type', 'number');
                ele.setAttribute('value', item[i - 1]);
                ele.disabled = true;
                actionDiv.appendChild(ele);
                const deletebutton = document.createElement('i');
                deletebutton.setAttribute('aria-hidden', 'true');
                deletebutton.setAttribute('class', 'fa fa-trash-o');
                deletebutton.setAttribute('id', 'deleteBtn');
                deletebutton.setAttribute('onclick', 'removeRow(this)');
                actionDiv.appendChild(deletebutton);
                const editbutton = document.createElement('i');
                editbutton.setAttribute('aria-hidden', 'true');
                editbutton.setAttribute('class', 'fa fa-pencil-square-o');
                editbutton.setAttribute('id', 'editBtn');
                editbutton.setAttribute('onclick', 'allowEdit(this)');
                actionDiv.appendChild(editbutton);
            } else {
                const ele = document.createElement('input');
                ele.setAttribute('type', i == 1 ? 'text' : 'number');
                if (i==1){ele.setAttribute('class','text-left');}
                ele.setAttribute('value', item[i - 1]);
                ele.disabled = true;
                td.appendChild(ele);
            }

        }
    });
    validateTotal();
    updateTotal();
}
const removeRow = (element) => {
    const index = element.parentNode.parentNode.parentNode.rowIndex;
    const invoiceTable = document.getElementById('invoice');
    invoiceTable.deleteRow(index);
    updateTotal(index - 1);
    items.splice(index - 1, 1);
    updateTable();
}
const allowEdit = (element) => {
    for (i = 1; i < 4; i++) {
        element.parentNode.parentNode.parentNode.children[i].children[0].disabled = false;
    }
    element.parentNode.parentNode.parentNode.children[1].children[0].focus();
    const savebutton = document.createElement('i');
    savebutton.setAttribute('aria-hidden', 'true');
    savebutton.setAttribute('class', 'fa fa-check');    
    savebutton.setAttribute('id', 'saveBtn');
    savebutton.setAttribute('onclick', 'saveEdit(this)');
    element.parentNode.appendChild(savebutton);
    element.parentNode.parentNode.children[0].children[2].remove();
}
const saveEdit = (element) => {
    const index = element.parentNode.parentNode.parentNode.rowIndex;
    const name = element.parentNode.parentNode.parentNode.children[1].children[0].value;
    const quantity = element.parentNode.parentNode.parentNode.children[2].children[0].value;
    const price = element.parentNode.parentNode.parentNode.children[3].children[0].value;
    if (validatePositivity(price, quantity)) {
        let tempArr = items.slice(0, index - 1);
        tempArr.push([name, quantity, price, quantity * price]);
        tempArr = [...tempArr, ...items.slice(index)];
        items = tempArr;
        updateTable();
        updateTotal();
    }
}
const updateTotal = (...index) => {
    const discount =parseInt(document.getElementById("discount").value);
    let vat = parseInt(document.getElementById("vat").value);
    const grandTotal = document.getElementById("grand-total");
    let sum = parseInt(document.getElementById("grand-total").value);
    vat=validateTax(vat);
        if (index.length == 0) {
            if (validateDiscount(discount)) {
                sum = 0;
                for (i = 0; i < items.length; i++) {
                    sum += items[i][3];
                }
                sum -= discount;
                if (sum < 0) {
                    sum += discount;
                    document.getElementById("discount").value = 0;
                    alert("Discount value too high!");
                }
                sum += sum * (vat / 100);
                grandTotal.value = "" + sum;
            }
        } else {
            sum -= items[index][3];
            grandTotal.value = ""+sum;
        }

}