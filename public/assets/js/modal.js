let modal;
const showDiv = () => {
    modal = document.getElementById("popup");
    modal.style.display = "flex";
}
const closeDiv = () => {
    modal.style.display = "none";
}
window.onclick = function (event) {
    if (event.target == modal) {
        closeDiv();
    }
}

const initialize = () => {
    n =  new Date();
    y = n.getFullYear();
    m = n.getMonth() + 1;
    d = n.getDate();
    document.getElementById("date").innerHTML = "Invoice Date:  "+ y + " - " + m + " - " + d;
}