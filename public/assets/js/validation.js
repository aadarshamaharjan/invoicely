const validateTotal = () => {
    if(document.getElementById('invoice').rows.length<2) {
        console.log(document.getElementById('invoice').rows.length);
        document.getElementById("discount").disabled=true;
        document.getElementById("vat").disabled=true;
    }else{
        document.getElementById("discount").disabled=false;
        document.getElementById("vat").disabled=false;
    }
}
const validateDiscount = (discount) => {
    if(discount<0){
        
        alert("Entered discount value is not acceptable!");
        document.getElementById("discount").value=0;
        return false;
    } 
        return true;
}
const validatePositivity = (price,quantity) => {
    if(parseInt(price)<0 || parseInt(quantity)<0){
        alert("Entered value is not acceptable!");
        return false;
    }
    else{
        return true;
    }
}
const validateTax = (vat) => {
    if(vat>100 || vat<0){
        alert("Entered vat rate is not acceptable!");
        document.getElementById("vat").value=0;
        return 0;
    }
    return vat;
}
const validateEmpty = (name,price,quantity) =>{
    if(name==""||price==""||quantity==""){
        alert("Empty fields not allowed!");
        return false;
    }
    return true;
}